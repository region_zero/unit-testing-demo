
# React Testing Library Examples

Use [Visual Studio Code](https://code.visualstudio.com/) (VS Code) and [Chrome Browser](https://www.google.com/chrome/) to develop unit tests for single page applications built using [Create React App](https://github.com/facebook/create-react-app), [Material UI](https://github.com/mui-org/material-ui), and [Redux ToolKit]([https://redux-toolkit.js.org/](https://redux-toolkit.js.org/)).

## Prerequisites

Install VS Code, and the recommended extensions for both VS  Code and Chrome.

### VS Code

[Download VS Code](https://code.visualstudio.com/download) for your OS of choice. VS Code features include: debugging, IntelliSense code completion, linting, etc. 

###  Install the following VS Code extensions:

Open VS Code. At the far left-hand side of the editor, locate and click the `Extensions` icon to display the extensions view. Search for and install:

- `Debugger for Chrome`

- `ESLint`

### Chrome Browser

Before we can debug our JavaScript code running in Chrome we will need to install the following browser extensions :

- [React Developer Tools](https://chrome.google.com/webstore/detail/react-developer-tools/fmkadmapgofadopljbjfkapdkoienihi)
- [Redux DevTools](https://chrome.google.com/webstore/detail/redux-devtools/lmhkpmbekcpmknklioeibfkpmmfibljd)


## How to Use This Repo

1. Run the app: `npm start`
2. View the app in a browser: [http://localhost:3000](http://localhost:3000) 
3. Read the page content and follow along in the associated test file.