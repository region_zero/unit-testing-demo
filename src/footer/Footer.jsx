import React from 'react';

const Footer = () => (
<div>
  <ul>
    <li>Commands to run tests:</li>
    <ul>
      <li>To run a single test: npm test -- &lt;testName&gt;,</li>
      <li>To to run all tests and continue watching for updates: npm test -- --watchAll,</li>
      <li>To run all tests one time: npm test -- --watchAll=false</li>
    </ul>
  </ul>
</div>
);

export default Footer;
