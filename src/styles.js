import { makeStyles } from '@material-ui/core/styles';
import theme from './theme';

const useStyles = makeStyles(() => ({
  text: {
    color: theme.palette.text.primary,
  },
  h1: {
    ...theme.typography.h1,
  },
}));

export default useStyles;
