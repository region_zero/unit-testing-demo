import React, { useState, useEffect } from 'react';
import { useHistory, useLocation } from 'react-router-dom';
// import {
//   IconButton, Tab, Tabs, Typography,
// } from '@material-ui/core';
import {
  Tab, Tabs,
} from '@material-ui/core';
// import { MenuRounded } from '@material-ui/icons';
import NAVIGATION_LINKS from './navigationLinks';

const Navigation = () => {
  const history = useHistory();
  const location = useLocation();

  const [currentTab, setCurrentTab] = useState(NAVIGATION_LINKS[0]);

  const handleTabChange = (nextTab) => {
    history.push(nextTab.route);
  };

  useEffect(() => {
    const newTab = NAVIGATION_LINKS.find((link) => location.pathname.includes(link.route))
      || NAVIGATION_LINKS[0];
    setCurrentTab(newTab);
  }, [location]);

  return (
    <Tabs
      indicatorColor="primary"
      textColor="primary"
      value={currentTab.id}
    >
      {NAVIGATION_LINKS.map((link) => (
        <Tab
          key={link.id}
          label={link.label}
          onClick={() => handleTabChange(link)}
          value={link.id}
        />
      ))}
    </Tabs>
  );
};

export default Navigation;
