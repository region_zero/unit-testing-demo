const NAVIGATION_LINKS = [
  {
    id: 0,
    label: 'Basic Snapshot',
    route: '/basic-snapshot',
  },
  {
    id: 1,
    label: 'Snapshot w/Props',
    route: '/snapshot-props',
  },
  {
    id: 2,
    label: 'Snapshot Non-Deterministic Data',
    route: '/snapshot-non-deterministic-data',
  },
  {
    id: 3,
    label: 'Run/Debug Tests',
    route: '/run-debug-tests',
  },
  {
    id: 4,
    label: 'Code Coverage',
    route: '/code-coverage',
  },
  {
    id: 5,
    label: 'Media Queries',
    route: '/media-queries',
  },
  {
    id: 6,
    label: 'Dropdowns',
    route: '/dropdowns',
  },
  {
    id: 7,
    label: 'Dialogs',
    route: '/dialogs',
  },
  {
    id: 8,
    label: 'Render w/Router',
    route: '/render-with-router',
  },
];

export default NAVIGATION_LINKS;
