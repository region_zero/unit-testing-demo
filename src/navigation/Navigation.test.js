import React from 'react';
import { Router } from 'react-router-dom';
import { createMemoryHistory } from 'history';
import { render } from '@testing-library/react';
import { ThemeProvider } from '@material-ui/core/styles';
import { CssBaseline } from '@material-ui/core';
import UserEvent from '@testing-library/user-event';
import Navigation from './Navigation';
import theme from '../theme';
import NAVIGATION_LINKS from './navigationLinks';

describe('<Navigation />', () => {
  const history = createMemoryHistory({ initialEntries: ['/basic-snapshot'] });
  const setUp = () => (
    render(
      <ThemeProvider theme={theme}>
        <CssBaseline />
        <Router history={history} >
          <Navigation />
        </Router>
      </ThemeProvider>,
    )
  );

  test('matches snapshot', () => {
    const { container } = setUp();
    expect(container).toMatchSnapshot();
  });

  test('tab links to correct page', () => {
    const { getByText } = setUp();

    expect(history.location.pathname).toBe('/basic-snapshot');
    const productOwnershipTab = getByText(/basic snapshot/i);
    expect(productOwnershipTab).toBeInTheDocument();

    NAVIGATION_LINKS.forEach((link) => {
      const navigationTab = getByText(link.label, 'i');
      expect(navigationTab).toBeInTheDocument();
      UserEvent.click(navigationTab);
      expect(history.location.pathname).toBe(link.route);
    });
  });
});
