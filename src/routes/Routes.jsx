import React from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';
import BasicSnapshot from '../app/basic-snapshot/BasicSnapshot';
import SnapshotProps from '../app/snapshot-props/SnapshotProps';
import SnapshotNonDeterministicData from '../app/snapshot-non-deterministic-data/SnapshotNonDeterministicData';
import Dropdowns from '../app/dropdowns/DropdownsConnector';
import Dialogs from '../app/dialogs/Dialogs';
import MediaQueries from '../app/media-queries/MediaQueries';
import CodeCoverage from '../app/code-coverage/CodeCoverage';
import RenderWithRouter from '../app/render-with-router/RenderWithRouter';
import RunDebugTests from '../app/run-debug-tests/RunDebugTests';
// import Search from '../app/search/SearchConnector';

const Routes = () => (
  <Switch>
    <Route path="/basic-snapshot">
      <BasicSnapshot />
    </Route>
    <Route path="/snapshot-props">
      <SnapshotProps name="Bingo" />
    </Route>
    <Route path="/snapshot-non-deterministic-data">
      <SnapshotNonDeterministicData />
    </Route>
    {/* <Route path="/search">
      <Search />
    </Route> */}
    <Route path="/run-debug-tests">
      <RunDebugTests />
    </Route>
    <Route path="/code-coverage">
      <CodeCoverage />
    </Route>
    <Route path="/dropdowns">
      <Dropdowns />
    </Route>
    <Route path="/dialogs">
      <Dialogs />
    </Route>
    <Route path="/media-queries">
      <MediaQueries />
    </Route>
    <Route path="/render-with-router">
      <RenderWithRouter />
    </Route>
    <Redirect from="/" to="/basic-snapshot" />
  </Switch>
);

export default Routes;
