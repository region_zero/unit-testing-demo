import React from 'react';
import { render } from '@testing-library/react';
import MediaQueries from './MediaQueries';
import createMatchMedia from '../testUtils/createMatchMedia';
import theme from '../../theme';

describe('<MediaQueries />', () => {
  test('matches snapshot', () => {
    const { container } = render(<MediaQueries />);
    expect(container).toMatchSnapshot();
  });

  test('renders mobile view', () => {
    const { getByText, queryByText } = render(<MediaQueries />);

    const mobileNavText = getByText(/mobile view/i);
    const desktopNavText = queryByText(/desktop view/i);

    expect(mobileNavText).toBeInTheDocument();
    expect(desktopNavText).not.toBeInTheDocument();
  });

  test('renders desktop view', () => {
    const windowMatchMedia = window.matchMedia; // store a reference to the original
    window.matchMedia = createMatchMedia(theme.breakpoints.values.lg); // set window.matchMedia

    const { getByText, queryByText } = render(<MediaQueries />);

    const mobileNavText = queryByText(/mobile view/i);
    const desktopNavText = getByText(/desktop view/i);

    expect(mobileNavText).not.toBeInTheDocument();
    expect(desktopNavText).toBeInTheDocument();
    window.matchMedia = windowMatchMedia; // reset window.matchMedia back to original
  });
});
