import React from 'react';
import { useMediaQuery } from '@material-ui/core';
import theme from '../../theme';
import useStyles from '../../styles';

const MediaQueries = () => {
  const classes = useStyles();
  const lgUp = useMediaQuery(theme.breakpoints.up('lg'));

  return (
    <>
      <div className={classes.h1}>Media Queries</div>
      <div className={classes.text}>
        <p>Notes:  </p>
        <ul>
          <li>Resize the browser window to see media queries in action below.</li>
          <li>testUtils/createMatchMedia.js</li>
          <ul>
            <li>jest renders mobile layout by default. To change the render size, use the test
            utility: createMatchMedia. The test utility implements matchMedia as described <a
            href="https://material-ui.com/components/use-media-query/#testing" target="_blank"
            rel="noopener noreferrer">here</a>.</li>
            <li><span aria-label="fire" role="img">🔥</span> Remember, when you set window.matchMedia, you also have to reset it back.
            Forgetting to do so will cause the next test to be rendered using the updated media
            query.</li>
            <li>For more information about createMatchMedia take a peek at the source code and
            accompanying code comments.
            </li>
          </ul>
        </ul>
        <p><span aria-label="wave" role="img">👋</span> Current viewing: {lgUp ? (
          <strong>desktop view</strong>
        ) : (
          <strong>mobile view</strong>
        )}</p>
      </div>
    </>
  );
};

export default MediaQueries;
