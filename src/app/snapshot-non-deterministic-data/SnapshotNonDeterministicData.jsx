import React from 'react';
import useStyles from '../../styles';

const SnapshotNonDeterministicData = () => {
  const classes = useStyles();
  // const currentDate = new Date();
  const currentDate = new Date(Date.now());

  return (
    <>
      <div className={classes.h1}>Snapshot with Non-Deterministic Data</div>
      <div className={classes.text}>
        <p>Running the same test, multiple times, on an unchanged component should produce the same
        snapshot every time. However, snapshots generated for components that contain data that
        is non-deterministic will cause a snapshot test to fail. Example of non-deterministic data
        include dates, uuids, etc. The snapshot test in SnapshotNonDeterministicData.test.js is
        currently failing due to call to create a new Date object. To get the test to pass,
        comment out line 15 and uncomment line 16 in SnapshotNonDeterministicData.jsx. Then, rerun
        the tests</p>
        <p>The reason the fix above works is because we are mocking Date.now(). Take a look at the
        mock in src/setupTests.js. setupTests.js is a special file in terms of testing. By including
        the mock in setupTests.js, the mock will intercept all calls to Date.now().</p>
        <div>The culprit: {currentDate.toString()}</div>
        <a href="https://jestjs.io/docs/en/snapshot-testing#2-tests-should-be-deterministic"
          target="_blank"
          rel="noopener noreferrer">
            reference
          </a>
      </div>
    </>
  );
};

export default SnapshotNonDeterministicData;
