import React from 'react';
import { render } from '@testing-library/react';
import SnapshotNonDeterministicData from './SnapshotNonDeterministicData';

describe('<SnapshotNonDeterministicData />', () => {
  test('matches snapshot', () => {
    const { container } = render(<SnapshotNonDeterministicData />);
    expect(container).toMatchSnapshot();
  });
});
