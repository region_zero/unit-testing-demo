import React from 'react';
import { render } from '@testing-library/react';
import RunDebugTests from './RunDebugTests';

describe('<RunDebugTests />', () => {
  test('matches snapshot', () => {
    const { container } = render(<RunDebugTests />);
    expect(container).toMatchSnapshot();
  });
});
