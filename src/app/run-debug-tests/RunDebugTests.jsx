import React from 'react';
import useStyles from '../../styles';

const RunDebugTests = () => {
  const classes = useStyles();

  return (
    <>
      <div className={classes.h1}>Run and Debug Tests</div>
      <div className={classes.text}></div>
    </>
  );
};

export default RunDebugTests;
