import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import useStyles from '../../styles';
import CustomAutocomplete from './custom-autocomplete/CustomAutocomplete';

const Dropdowns = (props) => {
  const classes = useStyles();
  const { isLoadingNames, names, onFetchNames } = props;
  const [name, setName] = useState('');

  useEffect(() => {
    onFetchNames();
  }, [onFetchNames]);

  const handleChange = (e) => {
    setName(e.target.textContent);
  };

  return (
    <>
      <div className={classes.h1}>Dropdowns</div>
      <div className={classes.text}>
        <p>autocomplete selection: {name}</p>
        <CustomAutocomplete
          handleChange={handleChange}
          isLoadingNames={isLoadingNames}
          options={names}
        />
      </div>
    </>
  );
};

Dropdowns.propTypes = {
  isLoadingNames: PropTypes.bool.isRequired,
  names: PropTypes.arrayOf(PropTypes.shape({
    label: PropTypes.string,
    value: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  })),
  onFetchNames: PropTypes.func.isRequired,
};

export default Dropdowns;
