import React from 'react';
import userEvent from '@testing-library/user-event';
import { render } from '@testing-library/react';
import Dropdowns from './Dropdowns';

const names = [
  { label: 'label 1', value: 1 },
  { label: 'label 2', value: 2 },
  { label: 'label 3', value: 3 },
];

const mockOnFetchNames = jest.fn();

const dropdownProps = {
  isLoadingNames: false,
  names,
  onFetchNames: mockOnFetchNames,
};

describe('<Dropdowns />', () => {
  test('matches snapshot', () => {
    const { container } = render(<Dropdowns {...dropdownProps} />);
    expect(container).toMatchSnapshot();
  });

  test('disables the autocomplete while data is loading', async () => {
    const updatedProps = { ...dropdownProps, isLoadingNames: true, names: [] };
    const { getByLabelText } = render(<Dropdowns {...updatedProps} />);
    const searchBar = getByLabelText(/combo box/i);

    expect(searchBar).toBeDisabled();
  });

  test('updates the page content based on the autocomplete selection', async () => {
    const selectedLabel = new RegExp(names[2].label);
    const { container, findByText, getByLabelText } = render(<Dropdowns {...dropdownProps} />);
    const searchBar = getByLabelText(/combo box/i);

    expect(searchBar).toBeInTheDocument();
    expect(container).not.toHaveTextContent(selectedLabel);

    userEvent.click(searchBar);
    const selectedOption = await findByText(selectedLabel);
    userEvent.click(selectedOption);
    expect(container).toHaveTextContent(selectedLabel);
  });
});
