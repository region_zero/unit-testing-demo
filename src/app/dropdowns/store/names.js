import { createSelector, createSlice } from '@reduxjs/toolkit';
import { productService } from '../../../http';

const initialState = {
  error: undefined,
  list: undefined,
  pendingFetch: false,
};

const reducers = {
  fetchNamesRequest(state) {
    state.pendingFetch = true;
  },
  fetchNamesSuccess(state, { payload }) {
    state.list = payload;
    state.pendingFetch = false;
  },
  fetchNamesError(state, { error }) {
    state.error = error;
    state.pendingFetch = false;
  },
};

const { actions, reducer } = createSlice({
  name: 'names',
  initialState,
  reducers,
});

const { fetchNamesRequest, fetchNamesSuccess, fetchNamesError } = actions;

export const fetchNames = () => (dispatch) => {
  dispatch(fetchNamesRequest());

  return productService
    .get('users')
    .then(({ data: names }) => {
      dispatch(fetchNamesSuccess(names));
    })
    .catch(({ message }) => dispatch(fetchNamesError(message)));
};

const listSelector = (state) => state.product.names.list;

export const pendingFetchNamesSelector = (state) => state.product.names.pendingFetch;

export const namesSelector = createSelector(
  listSelector,
  (names) => names?.map(
    ({ id, name }) => ({
      label: name,
      value: id,
    })
  ),
);

export default reducer;
