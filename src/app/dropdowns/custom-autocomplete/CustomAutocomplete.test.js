import React from 'react';
import userEvent from '@testing-library/user-event';
import { render } from '@testing-library/react';
import CustomAutocomplete from './CustomAutocomplete';

const mockHandleChange = jest.fn();
const options = [
  { label: 'label 1', value: 1 },
  { label: 'label 2', value: 2 },
  { label: 'label 3', value: 3 },
];

const customAutocompleteProps = {
  handleChange: mockHandleChange,
  isLoadingNames: false,
  options,
};

describe('<CustomAutocomplete />', () => {
  const setUp = (props) => render(<CustomAutocomplete {...props} />);

  test('matches snapshot', () => {
    const { container } = setUp(customAutocompleteProps);
    expect(container).toMatchSnapshot();
  });

  test('calls onChange function passed in a prop', async () => {
    const { getByLabelText, findByText } = setUp(customAutocompleteProps);
    const autocomplete = getByLabelText(/combo box/i);
    expect(autocomplete).toBeInTheDocument();

    // sanity check, has this function been called prior to our interaction
    expect(mockHandleChange).not.toHaveBeenCalled();
    userEvent.click(autocomplete);

    // wait for the options to be rendered
    const selectedOption = await findByText(new RegExp(options[2].label));
    userEvent.click(selectedOption);

    // check if the change handler was triggered
    expect(mockHandleChange).toHaveBeenCalledTimes(1);
  });
});
