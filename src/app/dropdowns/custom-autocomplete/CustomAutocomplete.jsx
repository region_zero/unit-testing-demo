import React from 'react';
import { Autocomplete } from '@material-ui/lab';
import { TextField } from '@material-ui/core';
import PropTypes from 'prop-types';

const CustomAutocomplete = (props) => {
  const { handleChange, isLoadingNames, options } = props;

  return (
    <Autocomplete
      disabled={isLoadingNames}
      getOptionLabel={(option) => option.label}
      id="combo-box-demo"
      onChange={handleChange}
      options={options}
      renderInput={(params) => <TextField {...params} label="Combo box" variant="outlined" />}
      style={{ width: 300 }}
    />
  );
};


CustomAutocomplete.propTypes = {
  handleChange: PropTypes.func.isRequired,
  isLoadingNames: PropTypes.bool.isRequired,
  options: PropTypes.arrayOf(PropTypes.shape({
    label: PropTypes.string,
    value: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  })),
};

CustomAutocomplete.defaultProps = {
  options: [],
};

export default CustomAutocomplete;
