import React from 'react';
import { render } from '@testing-library/react';
import CodeCoverage from './CodeCoverage';

describe('<CodeCoverage />', () => {
  test('matches snapshot', () => {
    const { container } = render(<CodeCoverage />);
    expect(container).toMatchSnapshot();
  });
});
