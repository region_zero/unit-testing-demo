import React from 'react';
import useStyles from '../../styles';

const CodeCoverage = () => {
  const classes = useStyles();
  return (
    <>
      <div className={classes.h1}>Code Coverage</div>
      <div className={classes.text}>
        <p>To run code coverage and continue watching for updates:
          npm test -- --coverage --watchAll,</p>
        <p>To run code coverage one time: npm test -- --coverage --watchAll=false</p>
        <p>How to read the code coverage report</p>
        <ul>
          <li><a href="https://medium.com/swlh/react-testing-using-jest-along-with-code-coverage-report-7454b5ba0236" target="_blank" rel="noopener noreferrer">
            reference
          </a></li>
          <li><a href="https://medium.com/@krishankantsinghal/how-to-read-test-coverage-report-generated-using-jest-c2d1cb70da8b" target="_blank" rel="noopener noreferrer">
            reference
          </a></li>
        </ul>
        <p>How to improve the results</p>
        <p>Re-run the code coverage report</p>
      </div>
    </>
  );
};

export default CodeCoverage;
