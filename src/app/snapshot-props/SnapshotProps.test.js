import React from 'react';
import { render } from '@testing-library/react';
import SnapshotProps from './SnapshotProps';

describe('<SnapshotProps />', () => {
  // at a minimum the props object must contain all required props
  const defaultProps = {
    name: 'Ruby',
  };

  test('matches snapshot', () => {
    const { container } = render(<SnapshotProps {...defaultProps} />);
    expect(container).toMatchSnapshot();
  });
});
