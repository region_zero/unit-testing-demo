import React from 'react';
import PropTypes from 'prop-types';
import useStyles from '../../styles';

const SnapshotProps = (props) => {
  const { name } = props;
  const classes = useStyles();

  return (
    <>
      <div className={classes.h1}>Snapshot with Render Prop</div>
      <div className={classes.text}>
        <p>SnapshotProp.test.js provides an example of a unit test for a simple component
        with props passed in. In the following paragraph, the name 'Bingo' is the prop passed into
        the component.</p>
      </div>
      <p>...and {name} was her name-o</p>
    </>
  );
};

SnapshotProps.propTypes = {
  name: PropTypes.string.isRequired,
};

export default SnapshotProps;
