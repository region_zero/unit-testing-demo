import React from 'react';
import { render } from '@testing-library/react';
import { ThemeProvider } from '@material-ui/core/styles';
import { MemoryRouter } from 'react-router-dom';
import { CssBaseline } from '@material-ui/core';
import PropTypes from 'prop-types';
import { configureStore } from '@reduxjs/toolkit';
import { Provider } from 'react-redux';
import theme from '../../theme';
import reducer from '../../store';

const store = configureStore({ reducer });

const Providers = ({ children }) => (
  <ThemeProvider theme={theme}>
    <CssBaseline />
    <Provider store={store}>
      <MemoryRouter>
        {children}
      </MemoryRouter>
    </Provider>
  </ThemeProvider>
);

Providers.propTypes = {
  children: PropTypes.node.isRequired,
};

const customRender = (ui, options) => render(ui, { wrapper: Providers, ...options });

export * from '@testing-library/react';
export { customRender as render };
