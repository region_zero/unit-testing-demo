import mediaQuery from 'css-mediaquery';

/**
 * This function is a polyfill for matchMedia. Currently jsDom does not implement the
 * function but the functionality is required by jest for testing components using
 * media queries. https://material-ui.com/components/use-media-query/#testing
 *
 * @param {number} width - Numeric value of width
 * @return {MediaQueryList} - new MediaQueryList object representing the parsed
 * results of the specified media query string
 *
 * @example
 *
 *     createMatchMedia(1200)
 */
const createMatchMedia = (width) => (
  (query) => ({
    matches: mediaQuery.match(query, { width }),
    addListener: () => {},
    removeListener: () => {},
  })
);

export default createMatchMedia;
