import React from 'react';
import { render } from '@testing-library/react';
import BasicSnapshot from './BasicSnapshot';

describe('<BasicSnapshot />', () => {
  test('matches snapshot', () => {
    const { container } = render(<BasicSnapshot />);
    expect(container).toMatchSnapshot();
  });
});
