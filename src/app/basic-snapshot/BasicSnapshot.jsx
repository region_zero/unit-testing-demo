import React from 'react';
import useStyles from '../../styles';

const BasicSnapshot = () => {
  const classes = useStyles();

  return (
    <>
      <div className={classes.h1}>Basic Snapshot</div>
      <div className={classes.text}>
        <p>To see the most basic of snapshot tests, take a look at BasicSnapshot.test.js.</p>
        <p>Notes about setting up tests in jest:  </p>
        <ul>
          <li>There are 2 type of blocks that make up a test:</li>
          <ul>
            <li>describe blocks</li>
            <ul>
              <li>used for grouping</li>
              <li>can be nested within other describe blocks</li>
              <li>the root describe block, should be named after the component it tests.</li>
              <ul>
                {/* <li>`example: describe('<BasicSnapshot />', () => {`</li> */}
                <li>the name of the root block for BasicSnapshot.test.js is: &lt;BasicSnapshot /
                &gt;</li>
              </ul>
            </ul>
            <li>test/it blocks</li>
            <ul>
              <li>each test is identified by it's name</li>
              <ul>
                <li>the only test in BasicSnapshot.test.js is named: matches snapshot</li>
                <li>avoid using special characters in test names</li>
              </ul>
              <li>each test block is required to contain at least one assertion (e.g expect)</li>
              <li>it is an alias for test <a href="https://jestjs.io/docs/en/api.html#testname-fn-timeout" target="_blank"
              rel="noopener noreferrer">reference</a></li>
            </ul>
          </ul>
        </ul>
        <p>Notes about snapshots:  </p>
        <ul>
          <li>The purpose of a snapshot is to make sure the UI does not change unexpectedly.</li>
          <li>Jest's snapshot functionality captures the rendered output of a component to a file.
          </li>
        </ul>
      </div>
    </>
  );
};

export default BasicSnapshot;
