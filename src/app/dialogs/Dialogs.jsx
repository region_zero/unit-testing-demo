import React, { useState } from 'react';
import {
  Button, Dialog, DialogActions, DialogContent, DialogTitle,
} from '@material-ui/core';
import useStyles from '../../styles';

const Dialogs = () => {
  const classes = useStyles();
  const [isOpen, setIsOpen] = useState(false);

  const handleClick = () => {
    setIsOpen(!isOpen);
  };

  const handleCancel = () => {
    setIsOpen(false);
  };

  const handleOk = () => {
    setIsOpen(false);
  };

  return (
    <>
      <div className={classes.h1}>Dialogs</div>
      <div className={classes.text}>
        <p>So, why are we testing this?</p>
        <Button onClick={handleClick} variant="contained">Show dialog</Button>
        <Dialog
          disableBackdropClick
          disableEscapeKeyDown
          maxWidth="xs"
          aria-labelledby="confirmation-dialog-title"
          open={isOpen}
        >
          <DialogTitle id="confirmation-dialog-title">Curabitur consectetur pharetra leo</DialogTitle>
          <DialogContent dividers>
            Proin quam arcu, dictum eget elit et, dignissim vehicula ligula. Nullam ac mauris augue.
          </DialogContent>
          <DialogActions>
            <Button autoFocus onClick={handleCancel} color="primary">
              Cancel
            </Button>
            <Button onClick={handleOk} color="primary">
              Ok
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    </>
  );
};

export default Dialogs;
