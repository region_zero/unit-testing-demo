import React from 'react';
import userEvent from '@testing-library/user-event';
import { render, screen, waitFor } from '@testing-library/react';
import Dialogs from './Dialogs';

describe('<Dialogs />', () => {
  test('matches snapshot', () => {
    const { container } = render(<Dialogs />);
    expect(container).toMatchSnapshot();
  });

  test('dialog closed by default', async () => {
    render(<Dialogs />);
    const dialogContainer = screen.queryByRole('dialog');
    expect(dialogContainer).not.toBeInTheDocument();
  });

  test('shows dialog button opens dialog', async () => {
    const { getByText } = render(<Dialogs />);

    const showDialogButton = getByText(/show dialog/i);
    userEvent.click(showDialogButton);

    const dialogContainer = screen.getByRole('dialog');
    expect(dialogContainer).toBeInTheDocument();
  });

  test('cancel button closes dialog', async () => {
    const { getByText } = render(<Dialogs />);

    const showDialogButton = getByText(/show dialog/i);
    userEvent.click(showDialogButton);

    const dialogContainer = screen.getByRole('dialog');
    expect(dialogContainer).toBeInTheDocument();

    const cancelButton = screen.getByText(/cancel/i);
    userEvent.click(cancelButton);

    await waitFor(() => {
      expect(screen.queryByRole('dialog')).not.toBeInTheDocument();
    });
  });

  test('clicking ok button closes dialog', async () => {
    const { getByText } = render(<Dialogs />);

    const showDialogButton = getByText(/show dialog/i);
    userEvent.click(showDialogButton);

    const dialogContainer = screen.getByRole('dialog');
    expect(dialogContainer).toBeInTheDocument();

    const okButton = screen.getByText(/ok/i);
    userEvent.click(okButton);

    await waitFor(() => {
      expect(screen.queryByRole('dialog')).not.toBeInTheDocument();
    });
  });
});
