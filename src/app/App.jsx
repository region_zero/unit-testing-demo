import React from 'react';
import Routes from '../routes/Routes';
import Navigation from '../navigation/Navigation';
import Footer from '../footer/Footer';

const App = () => (
  <header>
    <Navigation />
    <main>
      <Routes />
    </main>
    <Footer />
  </header>
);

export default App;
