import React from 'react';
import userEvent from '@testing-library/user-event';
import { render, screen, waitFor } from '../testUtils/renderWithRouter';
import Search from './Search';

const names = [
  { label: 'label 1', value: 1 },
  { label: 'label 2', value: 2 },
  { label: 'label 3', value: 3 },
];

const mockOnFetchNames = jest.fn();

const searchProps = {
  isLoadingNames: false,
  names,
  onFetchNames: mockOnFetchNames,
};

describe('<Search />', () => {
  const setUp = (props) => render(<Search {...props} />);

  test('matches snapshot', () => {
    const { container } = setUp(searchProps);
    expect(container).toMatchSnapshot();
  });

  test('disables the autocomplete while data is loading', async () => {
    const updatedProps = { ...searchProps, isLoadingNames: true, names: [] };
    const { getByLabelText } = setUp(updatedProps);
    const searchBar = getByLabelText(/combo box/i);

    expect(searchBar).toBeDisabled();
  });

  test('updates the page content based on the autocomplete selection', async () => {
    const selectedLabel = new RegExp(names[2].label);
    const { container, findByText, getByLabelText } = setUp(searchProps);
    const searchBar = getByLabelText(/combo box/i);

    expect(searchBar).toBeInTheDocument();
    expect(container).not.toHaveTextContent(selectedLabel);

    userEvent.click(searchBar);
    const selectedOption = await findByText(selectedLabel);
    userEvent.click(selectedOption);
    expect(container).toHaveTextContent(selectedLabel);
  });

  test('dialog closed by default', async () => {
    setUp(searchProps);
    const dialogContainer = screen.queryByRole('dialog');
    expect(dialogContainer).not.toBeInTheDocument();
  });

  test('shows dialog button opens dialog', async () => {
    const { getByText } = setUp(searchProps);

    const showDialogButton = getByText(/show dialog/i);
    userEvent.click(showDialogButton);

    const dialogContainer = screen.getByRole('dialog');
    expect(dialogContainer).toBeInTheDocument();
  });

  test('cancel button closes dialog', async () => {
    const { getByText } = setUp(searchProps);

    const showDialogButton = getByText(/show dialog/i);
    userEvent.click(showDialogButton);

    const dialogContainer = screen.getByRole('dialog');
    expect(dialogContainer).toBeInTheDocument();

    const cancelButton = screen.getByText(/cancel/i);
    userEvent.click(cancelButton);

    await waitFor(() => {
      expect(screen.queryByRole('dialog')).not.toBeInTheDocument();
    });
  });

  test('clicking ok button closes dialog', async () => {
    const { getByText } = setUp(searchProps);

    const showDialogButton = getByText(/show dialog/i);
    userEvent.click(showDialogButton);

    const dialogContainer = screen.getByRole('dialog');
    expect(dialogContainer).toBeInTheDocument();

    const okButton = screen.getByText(/ok/i);
    userEvent.click(okButton);

    await waitFor(() => {
      expect(screen.queryByRole('dialog')).not.toBeInTheDocument();
    });
  });
});
