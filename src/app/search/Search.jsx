import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Link } from 'react-router-dom';
import {
  Button, Dialog, DialogActions, DialogContent, DialogTitle,
} from '@material-ui/core';
import PropTypes from 'prop-types';
import SearchBar from './search-bar/SearchBar';

const useStyles = makeStyles((theme) => ({
  text: {
    color: theme.palette.text.primary,
  },
  h1: {
    ...theme.typography.h1,
  },
}));

const Search = (props) => {
  const { isLoadingNames, names, onFetchNames } = props;
  const classes = useStyles();
  const [isOpen, setIsOpen] = useState(false);

  const [name, setName] = useState('');

  const currentDate = new Date(Date.now());

  useEffect(() => {
    onFetchNames();
  }, [onFetchNames]);

  const handleChange = (e) => {
    setName(e.target.textContent);
  };

  const handleClick = () => {
    setIsOpen(!isOpen);
  };

  const handleCancel = () => {
    setIsOpen(false);
  };

  const handleOk = () => {
    setIsOpen(false);
  };


  return (
    <>
      <div className={classes.h1}>Search</div>
      <SearchBar handleChange={handleChange} isLoadingNames={isLoadingNames} options={names} />
      <div>{name}</div>
      <Button variant="contained">{currentDate.toString()}</Button>
      <Button onClick={handleClick} variant="contained">Show dialog</Button>
      <Link to="/basic-snapshot">Go Basic Snapshot</Link>
      <Dialog
        disableBackdropClick
        disableEscapeKeyDown
        maxWidth="xs"
        aria-labelledby="confirmation-dialog-title"
        open={isOpen}
      >
        <DialogTitle id="confirmation-dialog-title">Curabitur consectetur pharetra leo</DialogTitle>
        <DialogContent dividers>
          Proin quam arcu, dictum eget elit et, dignissim vehicula ligula. Nullam ac mauris augue.
        </DialogContent>
        <DialogActions>
          <Button autoFocus onClick={handleCancel} color="primary">
            Cancel
          </Button>
          <Button onClick={handleOk} color="primary">
            Ok
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
};

Search.propTypes = {
  isLoadingNames: PropTypes.bool.isRequired,
  names: PropTypes.arrayOf(PropTypes.shape({
    label: PropTypes.string,
    value: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  })),
  onFetchNames: PropTypes.func.isRequired,
};

Search.defaultProps = {
  names: [],
};

export default Search;
