import React from 'react';
import userEvent from '@testing-library/user-event';
import { render } from '@testing-library/react';
import SearchBar from './SearchBar';

const mockHandleChange = jest.fn();
const options = [
  { label: 'label 1', value: 1 },
  { label: 'label 2', value: 2 },
  { label: 'label 3', value: 3 },
];

const searchBarProps = {
  handleChange: mockHandleChange,
  isLoadingNames: false,
  options,
};

describe('<SearchBar />', () => {
  const setUp = (props) => render(<SearchBar {...props} />);

  test('matches snapshot', () => {
    const { container } = setUp(searchBarProps);
    expect(container).toMatchSnapshot();
  });

  test('renders autocomplete', async () => {
    // using jsdom-sixteen we no longer have to mock createRange()
    const { getByLabelText, findByText } = setUp(searchBarProps);
    const autocomplete = getByLabelText(/combo box/i);
    expect(autocomplete).toBeInTheDocument();

    expect(mockHandleChange).not.toHaveBeenCalled();
    userEvent.click(autocomplete);
    const selectedOption = await findByText(new RegExp(options[2].label));
    userEvent.click(selectedOption);
    expect(mockHandleChange).toHaveBeenCalledTimes(1);
  });
});
