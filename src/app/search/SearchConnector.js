import { connect } from 'react-redux';
import { fetchNames, namesSelector, pendingFetchNamesSelector } from './store/names';
import Search from './Search';

const mapState = (state) => ({
  names: namesSelector(state),
  isLoadingNames: pendingFetchNamesSelector(state),
});

const mapDispatch = (dispatch) => ({
  onFetchNames: () => dispatch(fetchNames()),
});

export default connect(mapState, mapDispatch)(Search);
