import { combineReducers } from '@reduxjs/toolkit';
import names from './names';

export default combineReducers({
  names,
});
