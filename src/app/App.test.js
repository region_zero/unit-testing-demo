import React from 'react';
import { Router } from 'react-router-dom';
import { render } from '@testing-library/react';
import { createMemoryHistory } from 'history';
import { ThemeProvider } from '@material-ui/core/styles';
import { CssBaseline } from '@material-ui/core';
import { Provider } from 'react-redux';
import { configureStore } from '@reduxjs/toolkit';
import { render as renderWithRouter } from './testUtils/renderWithRouter';
import App from './App';
import theme from '../theme';
import NAVIGATION_LINKS from '../navigation/navigationLinks';
import reducer from '../store';

describe('<App />', () => {
  test('matches snapshot', () => {
    const { container } = renderWithRouter(<App />);
    // const { container } = render(<App />);
    expect(container).toMatchSnapshot();
  });

  test('renders page associated with the first item in navigation', () => {
    const history = createMemoryHistory();
    const store = configureStore({ reducer });

    render(
      <ThemeProvider theme={theme}>
        <CssBaseline />
        <Provider store={store}>
          <Router history={history}>
            <App />
          </Router>
        </Provider>
      </ThemeProvider>,
    );

    expect(history.location.pathname).toBe(NAVIGATION_LINKS[0].route);
  });
});
