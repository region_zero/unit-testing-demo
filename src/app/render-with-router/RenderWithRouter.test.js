import React from 'react';
import { render } from '@testing-library/react';
import RenderWithRouter from './RenderWithRouter';

describe('<RenderWithRouter />', () => {
  test('matches snapshot', () => {
    const { container } = render(<RenderWithRouter />);
    expect(container).toMatchSnapshot();
  });
});
