import React from 'react';
import useStyles from '../../styles';

const RenderWithRouter = () => {
  const classes = useStyles();
  return (
    <>
      <div className={classes.h1}>Render with Router</div>
      <div className={classes.text}>See App.test.jsx</div>
    </>
  );
};

export default RenderWithRouter;
