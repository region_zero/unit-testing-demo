import { combineReducers } from '@reduxjs/toolkit';
import product from '../app/search/store';
import dropdown from '../app/dropdowns/store';

export default combineReducers({
  product,
  dropdown,
});
