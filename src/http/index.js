import axios from 'axios';

const productService = axios.create({
  baseURL: 'https://jsonplaceholder.typicode.com/', // process.env.PRODUCT_BASE_URL,
});

// eslint-disable-next-line import/prefer-default-export
export { productService };
