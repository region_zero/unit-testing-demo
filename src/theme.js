import { createMuiTheme } from '@material-ui/core/styles';

const theme = createMuiTheme({
  palette: {
    text: {
      primary: '#707070',
      secondary: 'purple',
    },
  },
  typography: {
    h1: {
      fontFamily: 'serif',
      fontWeight: 500,
      fontSize: 60,
      color: 'red',
    },
  },
});

export default theme;
